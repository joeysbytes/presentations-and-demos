echo ""
echo "Memory Statistics"
echo "-----------------"
cat /proc/meminfo|grep MemTotal
cat /proc/meminfo|grep MemFree

echo ""
echo "Disk Statistics"
echo "---------------"
df -Th|head -1
df -Th|grep /dev/sda1

echo ""
echo "Processes"
echo "---------"
PROCESS_COUNT=`ps -ef|wc -l`
echo "Number of running processes: ${PROCESS_COUNT}"

echo ""