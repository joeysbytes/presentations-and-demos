#!/bin/bash

xhost +
docker run \
    --rm \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v /tmp2/freemind-data/:/data/ \
    --name freemind \
    -ti eferro/freemind /freemind.sh $(id -u) $*
    
