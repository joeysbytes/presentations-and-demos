xhost +
docker run -d \
	--net host \
	-v /etc/localtime:/etc/localtime:ro \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	-v /tmp2/thunderbird-data:/root/.mozilla \
	-v /tmp2/thunderbird-data2:/root/.thunderbird \
	-e DISPLAY=unix$DISPLAY \
	--device /dev/snd \
	--device /dev/dri \
	--name thunderbird \
	jgiovaresco/thunderbird

