# docker exec -it vscode su user -c /usr/bin/code

docker run -d \
	-v /etc/localtime:/etc/localtime:ro \
	-v /tmp/.X11-unix:/tmp/.X11-unix  \
	-v /tmp2/vscode-data:/home/user \
	-e "DISPLAY=unix${DISPLAY}" \
	--name vscode \
	jess/vscode


