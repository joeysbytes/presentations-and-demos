#!/bin/bash

su user -c /usr/bin/code
su user -c "/usr/bin/code --install-extension donjayamanne.python"
su user -c "/usr/bin/code --install-extension DavidAnson.vscode-markdownlint"
su user -c "/usr/bin/code --install-extension PeterJausovec.vscode-docker"
su user -c "/usr/bin/code --install-extension Kasik96.format-php"
su user -c /usr/bin/code

sleep infinity

