# docker exec -it vscode2 su user -c /usr/bin/code

docker run -d \
	-v /etc/localtime:/etc/localtime:ro \
	-v /tmp/.X11-unix:/tmp/.X11-unix  \
	-v /tmp2/vscode2-data:/home/user \
	-v /tmp2/vscode2-work:/data \
	-e "DISPLAY=unix${DISPLAY}" \
	--name vscode2 \
	joeysbytes/vscode

