import turtle as turtle_library

# This is our screen variable
screen = turtle_library.getscreen()
# Change to logo programming mode
screen.mode("logo")

# This is our turtle variable
fred = turtle_library.getturtle()
# Set how fast the turtle draws
fred.speed(3)
# Make the turtle look like a turtle
fred.shape("turtle")

# Hide the turtle so we can see our beautiful picture
# fred.hideturtle()

square_size = 100
number_of_sides = 7
angle = 360 / number_of_sides

print(f"Angle: {angle}")

print("Angle: " + angle)

fred.pendown()

for side in range(number_of_sides):
    print(f"Drawing side: {side}")
    fred.forward(square_size)
    fred.right(angle)

fred.penup()

print(fred.position())



# We are done drawing, when we click the mouse, exit the program
screen.exitonclick()
