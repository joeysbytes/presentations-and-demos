import turtle as turtle_library

# This is our screen variable
screen = turtle_library.getscreen()
# Change to logo programming mode
screen.mode("logo")

# This is our turtle variable
leonardo = turtle_library.getturtle()
# Set how fast the turtle draws
leonardo.speed(1)
# Make the turtle look like a turtle
leonardo.shape("turtle")

# Draw the square
leonardo.forward(100)
leonardo.right(90)
leonardo.forward(100)
leonardo.right(90)
leonardo.forward(100)
leonardo.right(90)
leonardo.forward(100)
leonardo.right(90)

# Hide the turtle so we can see our beautiful picture
# leonardo.hideturtle()

# We are done drawing, when we click the mouse, exit the program
screen.exitonclick()
