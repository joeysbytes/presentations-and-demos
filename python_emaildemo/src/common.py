def get_password():
    file_name = "/tmp2/joeysbytesdev.txt"
    f = open(file_name, 'r')
    password = f.readline()
    f.close()
    return password

def pause(do_pause):
    if do_pause:
        print()
        input("Press ENTER to continue...")
        print()
    else:
        print()
