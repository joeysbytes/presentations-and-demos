# imapclient and pyzmail are 3rd-party Python libraries

from common import *
from pprint import pprint
import imapclient
import imaplib
import pyzmail

do_pause = True

# Email properties
imaplib._MAXLINE = 10000000 # increase maximum bytes allowed to be retrieved
imap_address = "imap.gmail.com"
imap_port = 993

# Login properties
email_login = "joeysbytesdev@gmail.com"

# Create imap object
imap_obj = imapclient.IMAPClient(imap_address, ssl=True)

# Login to imap server
imap_obj.login(email_login, get_password())

# List available folders
folder_list = imap_obj.list_folders()
pprint("Folder List: " + str(folder_list))
pause(do_pause)

# Select folder to search
imap_obj.select_folder('INBOX', readonly=True)

# Select emails to work with, returns UIDs of messages
message_ids = imap_obj.search([ 'ALL' ])
print("Message IDs: " + str(message_ids))
pause(do_pause)

# Fetch email(s)
# 430 = plain text email
# 429 = html, java magazine email
# raw_email is in RFC 822 format, made for IMAP to read
email_ids = [ 429 ]
raw_emails = imap_obj.fetch(email_ids, ['BODY[]'])
pprint("Raw Emails: " + str(raw_emails))
pause(do_pause)

# Put email into useable object
my_email = pyzmail.PyzMessage.factory(raw_emails[ email_ids[0] ][ b'BODY[]' ])
print(str(my_email))
pause(do_pause)

# Extract specific email fields
print("Subject: " + my_email.get_subject())
print("From: " + str(my_email.get_addresses('from')))
print("To: " + str(my_email.get_addresses('to')))
print("CC: " + str(my_email.get_addresses('cc')))
print("BCC: " + str(my_email.get_addresses('bcc')))
pause(do_pause)

# Extract plain text message body, if exists
my_email_text = ""
if my_email.text_part != None:
    print("PLAIN TEXT EMAIL PART")
    my_email_text = my_email.text_part.get_payload().decode('UTF-8')
    print(my_email_text)
    pause(do_pause)

# Extract html text message body, if exists
my_html_text = ""
if my_email.html_part != None:
    print("HTML TEXT EMAIL PART")
    my_email_html = my_email.html_part.get_payload().decode('UTF-8')
    print(my_email_html)
    pause(do_pause)

# Log out of imap server
imap_obj.logout()
