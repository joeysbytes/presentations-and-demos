from common import *
from pprint import pprint
import imaplib

do_pause = True

# Email properties
imaplib._MAXLINE = 10000000 # increase maximum bytes allowed to be retrieved
imap_address = "imap.gmail.com"
imap_port = 993

# Login properties
email_login = "joeysbytesdev@gmail.com"

# Create imap object
imap_obj = imaplib.IMAP4_SSL(host=imap_address, port=imap_port)

# Login to imap server
imap_obj.login(email_login, get_password())

# List available folders
print("Folder List: " + str(imap_obj.list()))
pause(do_pause)

# Select folder to search
imap_obj.select(mailbox="INBOX", readonly=True)

# Select emails to work with, returns UIDs of messages
status, message_numbers = imap_obj.search(None, 'ALL')
print("Search Status: " + str(status))
print("Message Numbers: " + str(message_numbers))
pause(do_pause)

# Fetch email
message_num = b'1'
status, data = imap_obj.fetch(message_num, '(RFC822)')
print("Status: " + str(status))
print("Email: " + str(data))
pause(do_pause)

# Log out of imap server
imap_obj.close()
imap_obj.logout()
