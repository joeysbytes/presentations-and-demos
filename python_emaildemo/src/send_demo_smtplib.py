# smtplib is a Python standard library
from common import *
import smtplib

# Email properties
smtp_address = "smtp.gmail.com"
smtp_tls_port = 587
# smtp_ssl_port = 465

# Login properties
email_login = "joeysbytesdev@gmail.com"

# Create smtp object
smtp_obj = smtplib.SMTP(smtp_address, smtp_tls_port)

# Send hello message to establish connection
smtp_obj.ehlo()

# Start tls encryption
smtp_obj.starttls()

# Log in to email
smtp_obj.login(email_login, get_password())

# Send email
from_email = "joeysbytesdev@gmail.com"
# to_email = [ "joeysbytesdev@gmail.com", "joey.rockhold@gmail.com" ] # can be a string
to_email = "Ryan.Anderson@fakeemail.com"
email_subject = "Demo email from Python"
email_text = "Hello Pythonians!"

email_msg = "Subject: " + email_subject + "\n" + email_text

failed_recipients = smtp_obj.sendmail(from_email, to_email, email_msg)
print("Failed recipients: " + str(failed_recipients))

# Disconnect from email
smtp_obj.quit()
